-- All item variants
local item_variants = {}

-- Pools where rolls should not yield variants
local excluded_pools = {}

callback("onItemRoll", function(pool, item)
	-- Do nothing if pool is excluded
	if excluded_pools[pool] then return item end
	
	-- Get all variants of the item
	local variants = item_variants[item]
	
	-- Do nothing if there are no variants
	if not variants then return item end
	
	-- Return a random variant of the item
	return table.irandom(variants)
end)

-- Stolen from apitools.lua
local function typeCheckError(fname, argn, aname, expected, got, level)
	error(string.format("bad argument #%i ('%s') to '%s' (%s expected, got %s)", argn, aname, fname, expected, typeOf(got)), 3 + (level or 0))
end

local function removeFromITable(t, value)
	local idx
	for i, v in pairs(t) do
		if v == value then
			idx = i
		end
	end
	if not idx then return end
	table.remove(t, idx)
end

-- Class
ItemVariety = {}

-- Function to register a new item variant
-- ====
-- variant  - the variant Item to be added
-- baseItem - the parent Item to register a variant of
function ItemVariety.registerItemVariant(variant, baseItem)
	-- Type checks
	if not isa(variant, "Item") then typeCheckError("ItemVariety.registerItemVariant", 1, "variant", "Item", variant) end
	if not isa(baseItem, "Item") then typeCheckError("ItemVariety.registerItemVariant", 2, "baseItem", "Item", baseItem) end
	
	-- Create the table if needed
	if not item_variants[baseItem] then
		item_variants[baseItem] = {baseItem}
	end
	
	-- Add the variant to the table
	table.insert(item_variants[baseItem], variant)
end

-- Function to remove an item variant
-- ====
-- variant  - the variant Item to be added
-- baseItem - (Optional) the parent Item to register a variant of. If empty,
--            removes variant from all items.
function ItemVariety.removeItemVariant(variant, baseItem)
	-- Type checks
	if not isa(variant, "Item") then typeCheckError("ItemVariety.removeItemVariant", 1, "variant", "Item", variant) end
	if baseItem ~= nil and not isa(baseItem, "Item") then typeCheckError("ItemVariety.removeItemVariant", 2, "baseItem", "Item", baseItem) end
	
	-- If we have a base item, remove the item from that one only
	if baseItem then
		removeFromITable(item_variants[baseItem], variant)
		if next(item_variants[baseItem]) == nil then
			item_variants[baseItem] = nil
		end
		return
	end
	
	-- Otherwise remove it from everywhere
	for bi, bit in pairs(item_variants) do
		removeFromITable(bit, variant)
		if next(bit) == nil then
			item_variants[bi] = nil
		end
	end
end

-- Function to remove all variants of an item
-- ====
-- baseItem - the item to remove all variants from
function ItemVariety.removeVariants(baseItem)
	if not isa(baseItem, "Item") then typeCheckError("ItemVariety.removeVariants", 1, "baseItem", "Item", baseItem) end
	
	item_variants[baseItem] = nil
end

-- Function to add an ItemPool to the exclude list, preventing
-- items rolled from that pool to have variants.
-- ====
-- itemPool - The ItemPool to exclude
function ItemVariety.excludePool(itemPool)
	if not isa(itemPool, "ItemPool") then typeCheckError("ItemVariety.excludePool", 1, "itemPool", "ItemPool", itemPool) end
	
	excluded_pools[itemPool] = true
end

-- Function to remove an ItemPool from the exclude list, if it was added
-- ====
-- itemPool - The ItemPool to reinclude
function ItemVariety.reincludePool(itemPool)
	if not isa(itemPool, "ItemPool") then typeCheckError("ItemVariety.reincludePool", 1, "itemPool", "ItemPool", itemPool) end
	
	excluded_pools[itemPool] = nil
end

export("ItemVariety")

-- An example file to show how you would register a new variant, disabled by default
-- require "example"
