Variety Library
===============
A library to add variety to Risk of Rain.

Current Features  
- **Item variants**  
  Add variants to items, choosing a random variant every time the item is rolled.

Documentation
=============

Item variants  
-------------  
`ItemVariety.registerItemVariant(Item variant, Item baseItem)`  
Registers a new item variant, making that item sometimes replace the base item.  
By default, the item itself is always added to its own pool of variants, however it can be removed with `ItemVariety.removeItemVariant` to make only variants show up.  
**Input parameters:**  
`variant` - the variant `Item` to be added  
`baseItem` - the parent `Item` to register a variant of  

`ItemVariety.removeItemVariant(Item variant, [Item baseItem])`  
Removes an item variant.  
If `baseItem` is set, removes the variant only from that item, otherwise removes it from all items.  
Does nothing if the item is not a variant.  
**Input parameters:**  
`variant` - the variant `Item` to be removed  
`baseItem` - *(Optional)* the parent `Item` to remove the variant from, or `nil`  

`ItemVariety.removeVariants(Item baseItem)`  
Removes all variants from an item.  
Does nothing if the item had no variants.  
**Input parameters:**  
`baseItem` - the parent `Item` to remove all variants from

`ItemVariety.excludePool(ItemPool itemPool)`  
Excludes rolls from a certain pool from having variants applied.  
Items in the pool can still have variants, but as long as the pool is the "source" of the random item, it will not have variants.  
Does nothing if the pool was already excluded.  
**Input parameters:**  
`itemPool` - the `ItemPool` to be excluded from variant rolls

`ItemVariety.reincludePool(ItemPool itemPool)`  
Reincludes a pool, letting it have variants again.  
Does nothing if the pool was not previously excluded.  
**Input parameters:**  
`itemPool` - the `ItemPool` to be reincluded for variant rolls
